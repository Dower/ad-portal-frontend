﻿function getData() {
    const userAction = async () => {
        const response = await fetch('https://localhost:44356/api/car', {
            method: 'GET'
        });
        const myJson = await response.json();
        cars(myJson);
        carList(myJson);
    }
    userAction();
}

function carList(cars) {

    cars.forEach(obj => {
        //num++;
        var manufacturer = obj.manufacturer;
        var model = obj.manufacturerModel;
        var engine = obj.engine;
        var fuel = obj.fuel;
        var birth = obj.manufactureDate;
        var transmission = obj.transmission.type;

        var imgPath = obj.image[0].path;
        console.log(imgPath);

        var carList = document.querySelector(".carList");
        var card = document.createElement("div");
        card.classList.add("card");
        //, "col-sm-12", "col-md-6", "col-lg-4", "col-xl-3");

        var img = document.createElement("img");
        img.classList.add("card-img-top");
        //if (obj.image != null) {
            //img.src = obj.img.url;
            //img.alt = omg.img.desc;
        //}else{
        img.src = imgPath;
        //}

        var body = document.createElement("div");
        body.classList.add("card-body");

        var title = document.createElement("h5");
        title.classList.add("card-title");
        title.innerHTML = manufacturer + " (" + model + ")";

        var p = document.createElement("p");
        p.classList.add("card-text", "cut-text");
        p.innerHTML = engine + " " + fuel + " " + birth + " " + transmission;

        body.appendChild(title);
        body.appendChild(p);

        card.appendChild(img);
        card.appendChild(body);

        carList.appendChild(card);
    });
}

function car() {
    document.querySelector(".search").innerHTML = '<div data="../search/car" ></div>';
}
function bike() {
    document.querySelector(".search").innerHTML = '<object type="text/html" data="../search/bike" ></object>';
}
function wheel() {
    document.querySelector(".search").innerHTML = '<object type="text/html" data="../search/wheel" ></object>';
}
function parts() {
    document.querySelector(".search").innerHTML = '<object type="text/html" data="../search/parts" ></object>';
}
function farm() {
    document.querySelector(".search").innerHTML = '<object type="text/html" data="../search/farm" ></object>';
}
function truck() {
    document.querySelector(".search").innerHTML = '<object type="text/html" data="../search/truck" ></object>';
}

//function onDelete(id) {
//    const url = 'https://localhost:44356/api/car' + id;
//    fetch(url, {
//        method: 'DELETE',
//        headers: {
//            'content-type': 'application/json'
//        }
//    })
//        .then(response => {
//            console.log(response);
//        })
//        .catch(err => {
//            console.log(err);
//        })
//}

//function onPost(json) {
//    fetch('https://localhost:44356/api/car', {
//        method: 'POST',
//        headers: {
//            'content-type': 'application/json'
//        },
//        body: json
//    })
//        .then(response => {
//            console.log(response)
//        })
//        .catch(err => {
//            console.log(err)
//        })
//}

//function onPut(id, json) {
//    const url = 'https://localhost:44356/api/car' + id;
//    fetch(url, {
//        method: 'PUT',
//        headers: {
//            'content-type': 'application/json'
//        },
//        //body: json
//    })
//        .then(response => {
//            console.log(response)
//        })
//        .catch(err => {
//            console.log(err)
//        })
//}