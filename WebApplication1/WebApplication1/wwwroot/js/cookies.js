﻿window.onload = auth();

function auth() {
    var ele;
    var parent = document.querySelector(".auth");

    if (checkCookie()) {
        ele = document.createElement("a");
        ele.href = "/user";
        ele.innerHTML = "Username";
    } else {
        ele = document.createElement("a");
        ele.href = "/login";
        ele.innerHTML = "Login";
    }
    parent.appendChild(ele);
}

function setCookie(JWT, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = "JWT=" + JWT + ";" + expires + ";path=/";
}

//function getCookie() {
//    var name = cname + "=";
//    var decodedCookie = decodeURIComponent(document.cookie);
//    var ca = decodedCookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(name) == 0) {
//            return c.substring(name.length, c.length);
//        }
//    }
//    return "";
//}

function checkCookie() {
    var JWT = getCookie("JWT");
    if (JWT != "") {
        return true;
    } else {
        return false;
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie() {
    var jwt = getCookie("JWT");
    if (jwt != "") {
        document.cookie = "JWT=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
    }
}