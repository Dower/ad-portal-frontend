﻿function register() {
    var userName = document.querySelector(".registerUserName").value;
    var name = document.querySelector(".registerName").value;
    var pass = document.querySelector(".registerPass").value;
    var pass2 = document.querySelector(".registerPass2").value;

    if (pass == pass2) {
        var json = { "Username": userName, "Name": name, "Password": pass };
        onRegister(json);
    }
    else { alert("Passwords do not match")}
}

function onRegister(json) {
   fetch('https://localhost:44356/api/register', {
        method: 'POST',
       headers: {
           'content-type': 'application/json',
        },  
        body: JSON.stringify(json)
   })
       .then(res => {
           if (!res.ok) {
               res.text().then(text => alert(JSON.parse(text).errors));
           }
           else {
               return res.json();
           }
       })
       .catch(err => {
           console.log(err);
       })
}