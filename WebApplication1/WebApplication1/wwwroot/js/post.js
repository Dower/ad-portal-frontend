﻿function post() {

    var carInfo = {
        "category": 0,
        "Manufacturer": "bmw",
        "ManufacturerModel": "asd",
        "manufactureDate": "2015-05-05T00:00:00",
        "engine": 3996,
        "Fuel": 0,
        "Frame": 0,
        "Color": "Zydra",
        "transmission": "Automatinė",
        "defects": "be defektu",
        "steeringWheelPos": true,
        "doors": '2',
        "seats": 4,
        "vehicleInspection": "2020-02-25T00:00:00",
        "description": "Gerai",
        "price": 20.00
    };

    var formData = new FormData();
    formData.append('obj', JSON.stringify(carInfo));
    var files = document.querySelector('input[type=file]').files;

    if (files) {
        for (let file of files) {
            formData.append('Image', file);
        }
    }

    onPost(formData);
}

function previewFile() {

    var files = document.querySelector('input[type=file]').files;
    var father = document.querySelector("#for_images");

    for (let file of files) {

        let img = document.createElement("img");
        img.style.height = "200px";
        img.src = URL.createObjectURL(file);
        father.appendChild(img);
    }
}

function onPost(formData) {

    fetch('https://localhost:44356/api/car', {
        method: 'POST',
        body: formData,
        headers: {
            'Authorization': 'Bearer ' + getCookie("JWT")
        }
    })
        .then(response => {
            console.log(response)
        })
        .catch(err => {
            console.log(err)
        });
}
