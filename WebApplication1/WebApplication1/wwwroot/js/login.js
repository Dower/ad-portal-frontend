﻿function login() {
    var userName = document.querySelector(".loginName").value;
    var pass = document.querySelector(".loginPass").value;

    var userInfo = { "Username": userName, "Password":pass };

    onLogin(userInfo);
}

function onLogin(json) {
    fetch('https://localhost:44356/api/login', {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            //'Authorization': 'Bearer ' + this.state.clientToken
        },
        body: JSON.stringify(json)
    })
        .then(res => {
            console.log(res);
            if (!res.ok) {
                res.text().then(text => alert(JSON.parse(text).errors));
            }
            else {
                res.text().then(text => {
                    setCookie(text);
                    console.log(text);
                    alert("Welcome");
                });
            }
        })
        .catch(err => {
            console.log(err);
        })
}